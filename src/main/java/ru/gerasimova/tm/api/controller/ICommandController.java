package ru.gerasimova.tm.api.controller;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void exit();

    void showHelp();

    void showCommands();

    void showArguments();

    void showInfo();

}
