package ru.gerasimova.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void registry();

    void viewUserProfile();

    void updateUserPassword();

}
