package ru.gerasimova.tm.api.service;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
