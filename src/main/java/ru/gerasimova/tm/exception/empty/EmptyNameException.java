package ru.gerasimova.tm.exception.empty;

public class EmptyNameException extends AbstractEmptyException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
