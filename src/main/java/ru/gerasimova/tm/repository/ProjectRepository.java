package ru.gerasimova.tm.repository;

import ru.gerasimova.tm.api.repository.IProjectRepository;
import ru.gerasimova.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId.equals(project.getUserId()))
            projects.remove(project);
        else return;
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> tasks = findAll(userId);
        this.projects.removeAll(tasks);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        for (final Project project : projects) {
            boolean idMatch = false;
            boolean userIdMatch = false;
            if (id.equals(project.getId())) idMatch = true;
            if (userId.equals(project.getUserId())) userIdMatch = true;
            if (idMatch && userIdMatch) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project : projects) {
            boolean nameMatch = false;
            boolean userIdMatch = false;
            if (name.equals(project.getName())) nameMatch = true;
            if (userId.equals(project.getUserId())) userIdMatch = true;
            if (nameMatch && userIdMatch) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) return null;
        remove(userId, project);
        return project;
    }

}
