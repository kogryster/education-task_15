package ru.gerasimova.tm.command.auth;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.entity.User;
import ru.gerasimova.tm.exception.empty.NullUserException;

public final class UserInfoCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-profile-view";
    }

    @Override
    public String description() {
        return "Info about current user.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        final String tempUserId = serviceLocator.getAuthService().getUserId();
        final User tempUser = serviceLocator.getUserService().findById(tempUserId);
        if (tempUser == null) throw new NullUserException();
        String currentUserLogin = "Login: " + tempUser.getLogin();
        String currentUserEmail = " E-mail: " + tempUser.getEmail();
        String currentUserId = " ID: " + tempUser.getId();
        String currentUserInfo = currentUserLogin + currentUserEmail + currentUserId;
        System.out.println(currentUserInfo);
        System.out.println("[OK]");
    }

}
