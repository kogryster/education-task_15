package ru.gerasimova.tm.command.auth;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.util.TerminalUtil;

public final class RegistrationCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String description() {
        return "Registry new user.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL]");
        final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
    }
}
