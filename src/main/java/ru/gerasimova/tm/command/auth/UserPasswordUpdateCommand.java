package ru.gerasimova.tm.command.auth;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.entity.User;
import ru.gerasimova.tm.util.TerminalUtil;

public final class UserPasswordUpdateCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-password-update";
    }

    @Override
    public String description() {
        return "Update password for current user.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PASSWORD]");
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        final User currentUser = serviceLocator.getUserService().findById(currentUserId);
        System.out.println("[ENTER NEW PASSWORD FOR " + currentUser.getLogin() + " ]");
        final String newPassword = TerminalUtil.nextLine();
        final User userNewPassword = serviceLocator.getUserService().updateUserPassword(currentUserId, newPassword);
        System.out.println("[OK]");
    }
}
