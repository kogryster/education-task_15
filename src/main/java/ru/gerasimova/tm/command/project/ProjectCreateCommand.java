package ru.gerasimova.tm.command.project;

import ru.gerasimova.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().create(userId, name, description);
        System.out.println("[OK]");
    }

}
