package ru.gerasimova.tm.command.project;

import ru.gerasimova.tm.entity.Project;
import ru.gerasimova.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-view-by-name";
    }

    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProjectInfo(project);
    }

}
