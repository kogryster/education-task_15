package ru.gerasimova.tm.command.system;

import ru.gerasimova.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ekaterina Gerasimova");
        System.out.println("E-MAIL: kogryster@gmail.com");
    }

}
