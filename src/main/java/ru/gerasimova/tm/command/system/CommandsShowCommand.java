package ru.gerasimova.tm.command.system;

import ru.gerasimova.tm.command.AbstractCommand;

import java.util.Arrays;

public final class CommandsShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        final String[] commands = serviceLocator.getCommandService().getCommands();
        System.out.println(Arrays.toString(commands));
    }

}
